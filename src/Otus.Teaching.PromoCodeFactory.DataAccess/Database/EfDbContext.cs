﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Database.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Database
{
    public class EfDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        public EfDbContext(DbContextOptions<EfDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new EmployeeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new PreferenceConfiguration());
            modelBuilder.ApplyConfiguration(new PromoCodeCodeConfiguration());
            // SeedDefaultData(modelBuilder);
        }

        //private void SeedDefaultData(ModelBuilder modelBuilder)
        //{
        //    //modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
        //    //var otherRoles = FakeDataFactory.Roles.Where(q => !FakeDataFactory.Employees.Any(d => d.Role.Equals(q.Id))).ToList();
        //    //modelBuilder.Entity<Role>().HasData(otherRoles);         
        //    modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
        //    //var otherPromocodes = FakeDataFactory.PromoCodes.Where(q => !FakeDataFactory.Customers.Any(d => d.Id.Equals(q.Customer?.Id))).ToList();
        //    //modelBuilder.Entity<PromoCode>().HasData(otherPromocodes);
        //    //var otherPreferences = FakeDataFactory.Preferences.Where(q => !FakeDataFactory.Customers.Any(d => d.Preferences.Any(d => d.Id.Equals(q.Id)))).ToList();

        //    //modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);



        //}

        public void SeedDefaultData()
        {
            var employees = FakeDataFactory.Employees.ToList();

            var roles = FakeDataFactory.Roles.ToList();
            var customers = FakeDataFactory.Customers.ToList();
            var preferences = FakeDataFactory.Preferences.ToList();
            //var promoCodes = FakeDataFactory.PromoCodes.ToList();

            //Seed elements
            var employeesBase = FakeDataFactory.Employees.ToList();
            employeesBase.ForEach(q => { q.Role = null; });
            var rolesBase = FakeDataFactory.Roles.ToList();
            var customersBase = FakeDataFactory.Customers.ToList();
            customersBase.ForEach(q => { q.PromoCodes = null; q.Preferences = null; });
            var preferencesBase = FakeDataFactory.Preferences.ToList();
            preferencesBase.ForEach(q => { q.Customers = null; });
            //var promoCodesBase = FakeDataFactory.PromoCodes.ToList();
            //promoCodesBase.ForEach(q => { q.Preference = null; q.Customer = null; q.PartnerManager = null; });

            Employees.AddRange(employeesBase);
            Roles.AddRange(rolesBase);
            Customers.AddRange(customersBase);
            Preferences.AddRange(preferencesBase);
            //PromoCodes.AddRange(promoCodesBase);
            SaveChanges();

            //Relationships
            var dbEmployees = Employees.ToList();
            dbEmployees.ForEach(dbEmployee =>
            {
                var employee = employees.SingleOrDefault(q => q.Id.Equals(dbEmployee.Id));
                if (employee != null)
                {
                    var dbRole = Roles.SingleOrDefault(q => q.Id.Equals(employee.Role.Id));
                    if (dbRole != null) dbEmployee.Role = dbRole;
                }
            });

            //var dbPromoCodes = PromoCodes.ToList();
            //dbPromoCodes.ForEach(dbPromoCode =>
            //{
            //    var promoCode = promoCodes.FirstOrDefault(q => q.Id.Equals(dbPromoCode.Id));
            //    if (promoCode != null)
            //    {
            //        if (promoCode.Preference != null)
            //        {
            //            var dbPreference = Preferences.SingleOrDefault(q => q.Id.Equals(promoCode.Preference.Id));
            //            if (dbPreference != null) dbPromoCode.Preference = dbPreference;
            //        }

            //        if (promoCode.PartnerManager != null)
            //        {
            //            var dbEmployee = Employees.SingleOrDefault(q => q.Id.Equals(promoCode.PartnerManager.Id));
            //            if (dbEmployee != null) dbPromoCode.PartnerManager = dbEmployee;
            //        }
            //    }
            //});

            var dbCustomers = Customers.ToList();
            dbCustomers.ForEach(dbCustomer =>
            {
                var customer = customers.FirstOrDefault(q => q.Id.Equals(dbCustomer.Id));
                if (customer != null)
                {
                    if (customer.Preferences.Any())
                    {
                        customer.Preferences.ForEach(p =>
                        {
                            var dbPreference = Preferences.SingleOrDefault(q => q.Id.Equals(p.Id));
                            if (dbPreference != null)
                            {
                                if (dbCustomer.Preferences == null) dbCustomer.Preferences = new List<Preference>();
                                dbCustomer.Preferences.Add(dbPreference);
                            }
                        });
                    }

                    //var customerPromoCodes = promoCodes.Where(q => q.Customer.Id.Equals(customer.Id)).ToList();
                    //if (customerPromoCodes.Any())
                    //{
                    //    customerPromoCodes.ForEach(p =>
                    //    {
                    //        var dbPromoCode = PromoCodes.SingleOrDefault(q => q.Id.Equals(p.Id));
                    //        if (dbPromoCode != null)
                    //        {
                    //            if (dbCustomer.PromoCodes == null) dbCustomer.PromoCodes = new List<PromoCode>();
                    //            dbCustomer.PromoCodes.Add(dbPromoCode);
                    //        }
                    //    });
                    //}
                }
            });

            SaveChanges();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.LogTo(Console.WriteLine, Microsoft.Extensions.Logging.LogLevel.Information);
    }
}
