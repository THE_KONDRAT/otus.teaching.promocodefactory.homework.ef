﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Database.Configuration
{
    internal class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable("Employees");
            builder.HasKey(q => q.Id);
            builder.Property(q => q.FirstName)
                .HasMaxLength(64);
            builder.Property(q => q.LastName)
                .HasMaxLength(64);
            builder.Ignore(q => q.FullName);
            builder.Property(q => q.Email)
                .HasMaxLength(64);
            //1 к 1 пока
            builder.HasOne(q => q.Role)
                .WithOne()
                .HasForeignKey<Employee>(/*q => q.RoleId*/"RoleId");
            //.HasForeignKey<Role>("EmployeeId");
        }
    }
}
