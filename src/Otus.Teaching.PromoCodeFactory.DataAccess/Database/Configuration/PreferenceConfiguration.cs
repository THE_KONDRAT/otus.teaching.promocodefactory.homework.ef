﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Database.Configuration
{
    internal class PreferenceConfiguration : IEntityTypeConfiguration<Preference>
    {
        public void Configure(EntityTypeBuilder<Preference> builder)
        {
            builder.ToTable("Preferences");
            builder.HasKey(q => q.Id);
            builder.Property(q => q.Name)
                .HasMaxLength(64);
            builder.HasMany<PromoCode>()
                .WithOne(q => q.Preference)
                .HasForeignKey("PreferenceId");
            //1 к 1
            //builder.HasOne<PromoCode>()
            //    .WithOne(q => q.Preference)
            //    .HasForeignKey<PromoCode>("PreferenceId");
        }
    }
}
