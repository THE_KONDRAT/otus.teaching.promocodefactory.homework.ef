﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Database.Configuration
{
    internal class PromoCodeCodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.ToTable("PromoCodes");
            builder.HasKey(q => q.Id);
            builder.Property(q => q.Code)
                .HasMaxLength(32);
            builder.Property(q => q.PartnerName)
                .HasMaxLength(256);
            builder.Property(q => q.ServiceInfo)
                .HasMaxLength(512);
            builder.HasOne(q => q.PartnerManager)
                .WithMany()
                .HasForeignKey("PartnerManagerId");
        }
    }
}
