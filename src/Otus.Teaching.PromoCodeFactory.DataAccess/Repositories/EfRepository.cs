﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Database;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public abstract class EfRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected EfDbContext _context { get; set; }

        public EfRepository(EfDbContext context)
        {
            _context = context;
        }
        
        public virtual async Task<IEnumerable<T>> GetAllAsync() => await _context.Set<T>().ToListAsync();

        public virtual async Task<T> GetByIdAsync(Guid id) => await _context.Set<T>().FirstOrDefaultAsync(q => q.Id.Equals(id));

        public virtual async Task<bool> IsExistsAsync(Guid id) => await _context.Set<T>().AnyAsync(q => q.Id.Equals(id));

        public virtual async Task<Guid> AddAsync(T entity)
        {
            var newId = Guid.NewGuid();
            entity.Id = newId;
            //entity.Id = null;
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity.Id;
        }

        public virtual async Task RemoveByIdAsync(Guid id)
        {
            var entity = await _context.Set<T>().FirstAsync(x => x.Id.Equals(id));
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
        }

        public virtual async Task UpdateAsync(T entity)
        {
            var entityToUpdate = await _context.Set<T>().FirstAsync(x => x.Id.Equals(entity.Id));
            var entityProperties = entity.GetType().GetProperties().Where(q => q.SetMethod != null && q.Name != nameof(BaseEntity.Id));
            foreach (var property in entityProperties)
            {
                var pType = property.PropertyType;
                if (pType.IsGenericType && pType.GetGenericTypeDefinition() == typeof(List<>))
                {
                    var list = property.GetValue(entity);
                    if (list != null) property.SetValue(entityToUpdate, list); //Чтобы стереть делаем clear.
                }
                else property.SetValue(entityToUpdate, property.GetValue(entity));
            }
            await _context.SaveChangesAsync();
        }
    }
}