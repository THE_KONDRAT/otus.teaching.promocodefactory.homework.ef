﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Database;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.EfRepositoryImplementations
{
    public class RoleRepository : EfRepository<Role>
    {
        public RoleRepository(EfDbContext context) : base(context) { }

        public override async Task UpdateAsync(Role entity)
        {
            var entityToUpdate = await GetByIdAsync(entity.Id);
            entityToUpdate.Name = entity.Name;
            entityToUpdate.Description = entity.Description;
            await _context.SaveChangesAsync();
        }
    }
}
