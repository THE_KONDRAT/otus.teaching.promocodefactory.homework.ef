﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Database;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.EfRepositoryImplementations
{
    public class PromoCodeRepository : EfRepository<PromoCode>
    {
        public PromoCodeRepository(EfDbContext context) : base(context) { }

        public override async Task<IEnumerable<PromoCode>> GetAllAsync() => 
            await _context.PromoCodes.Include(q => q.PartnerManager).Include(q => q.Customer).Include(q => q.Preference).ToListAsync();

        public override async Task<PromoCode> GetByIdAsync(Guid id) 
            => await _context.PromoCodes.Include(q => q.PartnerManager).Include(q => q.Customer).Include(q => q.Preference).FirstOrDefaultAsync(q => q.Id.Equals(id));
        
        public override async Task UpdateAsync(PromoCode entity)
        {
            var entityToUpdate = await GetByIdAsync(entity.Id);
            entityToUpdate.Code = entity.Code;
            entityToUpdate.PartnerName = entity.PartnerName;
            entityToUpdate.PartnerManager = entity.PartnerManager;
            entityToUpdate.Customer = entity.Customer;
            entityToUpdate.ServiceInfo = entity.ServiceInfo;
            entityToUpdate.Preference = entity.Preference;
            entityToUpdate.BeginDate = entity.BeginDate;
            entityToUpdate.EndDate = entity.EndDate;
            await _context.SaveChangesAsync();
        }
    }
}
