﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Database;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.EfRepositoryImplementations
{
    public class EmployeeRepository : EfRepository<Employee>
    {
        public EmployeeRepository(EfDbContext context) : base(context) { }

        public override async Task<IEnumerable<Employee>> GetAllAsync() => await _context.Employees.Include(q => q.Role).ToListAsync();

        public override async Task<Employee> GetByIdAsync(Guid id) => await _context.Employees.Include(q => q.Role).FirstOrDefaultAsync(q => q.Id.Equals(id));
        
        public override async Task UpdateAsync(Employee entity)
        {
            var entityToUpdate = await GetByIdAsync(entity.Id);
            entityToUpdate.FirstName = entity.FirstName;
            entityToUpdate.LastName = entity.LastName;
            entityToUpdate.Email = entity.Email;
            entityToUpdate.AppliedPromocodesCount = entity.AppliedPromocodesCount;
            entityToUpdate.Role = entity.Role;
            await _context.SaveChangesAsync();
        }
    }
}
