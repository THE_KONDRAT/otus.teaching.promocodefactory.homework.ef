﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Database;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.EfRepositoryImplementations
{
    public class CustomerRepository : EfRepository<Customer>
    {
        public CustomerRepository(EfDbContext context) : base(context) { }

        public override async Task<IEnumerable<Customer>> GetAllAsync() => await _context.Customers.Include(q => q.Preferences).Include(q => q.PromoCodes).ToListAsync();

        public override async Task<Customer> GetByIdAsync(Guid id) => await _context.Customers.Include(q => q.Preferences).Include(q => q.PromoCodes).FirstOrDefaultAsync(q => q.Id.Equals(id));
        
        public override async Task UpdateAsync(Customer entity)
        {
            var entityToUpdate = await GetByIdAsync(entity.Id);
            entityToUpdate.FirstName = entity.FirstName;
            entityToUpdate.LastName = entity.LastName;
            entityToUpdate.Email = entity.Email;
            entityToUpdate.PromoCodes = entity.PromoCodes;
            entityToUpdate.Preferences = entity.Preferences;
            await _context.SaveChangesAsync();
        }
    }
}
