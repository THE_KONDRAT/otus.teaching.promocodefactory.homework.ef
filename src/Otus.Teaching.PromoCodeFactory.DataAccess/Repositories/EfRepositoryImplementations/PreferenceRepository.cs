﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Database;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.EfRepositoryImplementations
{
    public class PreferenceRepository : EfRepository<Preference>
    {
        public PreferenceRepository(EfDbContext context) : base(context) { }

        public override async Task<IEnumerable<Preference>> GetAllAsync() => await _context.Preferences.Include(q => q.Customers).ToListAsync();

        public override async Task<Preference> GetByIdAsync(Guid id) => await _context.Preferences.Include(q => q.Customers).FirstOrDefaultAsync(q => q.Id.Equals(id));
        
        public override async Task UpdateAsync(Preference entity)
        {
            var entityToUpdate = await GetByIdAsync(entity.Id);
            entityToUpdate.Name = entity.Name;
            entityToUpdate.Customers= entity.Customers;
            await _context.SaveChangesAsync();
        }
    }
}
