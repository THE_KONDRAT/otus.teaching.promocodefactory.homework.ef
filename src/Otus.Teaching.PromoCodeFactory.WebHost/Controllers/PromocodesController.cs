﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository, IRepository<Customer> customerRepository, IRepository<Employee> employeeRepository,
            IRepository<Preference> preferenceRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync() => 
            Ok((await _promoCodeRepository.GetAllAsync()).Select(q => new PromoCodeShortResponse
            {
                Id = q.Id,
                Code = q.Code,
                PartnerName = q.PartnerName,
                ServiceInfo = q.ServiceInfo,
                BeginDate = q.BeginDate.ToString("d"),
                EndDate = q.EndDate.ToString("d")
            }).ToList());
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var manager = await _employeeRepository.GetByIdAsync(request.PartnerManagerId);
            if (manager == null) return BadRequest(request.PartnerManagerId);
            
            //Пока поиск по названию
            var preference = (await _preferenceRepository.GetAllAsync()).FirstOrDefault(q => q.Name.ToLower().Equals(request.Preference.ToLower())); //Сделать AsQueryable
            //var preference = await _preferenceRepository.GetByIdAsync(Guid.Parse(request.Preference)); //Сделать AsQueryable
            if (preference == null) return BadRequest(request.Preference);
            
            if (!DateTime.TryParse(request.BeginDate, out DateTime beginDate)) return BadRequest(request.BeginDate);
            if (!DateTime.TryParse(request.EndDate, out DateTime endDate)) return BadRequest(request.EndDate);

            var customers = (await _customerRepository.GetAllAsync())
                .Where(q => q.Preferences.FirstOrDefault(q => q.Id.Equals(preference.Id)) != null).ToList(); //Сделать AsQueryable
            var luckyCustomer = customers.Skip(RandomNumberGenerator.GetInt32(0, customers.Count)).Take(1).First();
            
            var newPromocode = new PromoCode
            {
                Code = request.PromoCode,
                PartnerManager = manager,
                PartnerName = request.PartnerName,
                Customer = luckyCustomer,
                Preference = preference,
                ServiceInfo = request.ServiceInfo,
                BeginDate = beginDate,
                EndDate = endDate,
                CreationDateUtc = DateTime.UtcNow,
            };

            var id = await _promoCodeRepository.AddAsync(newPromocode);

            luckyCustomer.PromoCodes.Add(newPromocode);
            await _customerRepository.UpdateAsync(luckyCustomer);

            manager.AppliedPromocodesCount++;
            await _employeeRepository.UpdateAsync(manager);

            return Ok(id);
        }
    }
}