﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;

        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync() => (await _customerRepository.GetAllAsync())
            .Select(q => new CustomerShortResponse
            {
                Id = q.Id,
                FirstName = q.FirstName,
                LastName = q.LastName,
                Email = q.Email,
            }).ToList();

        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(CustomerResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var result = await _customerRepository.GetByIdAsync(id);
            if (result == null) return NotFound();
            return Ok(new CustomerResponse
            {
                Id = result.Id,
                FirstName = result.FirstName,
                LastName = result.LastName,
                Email = result.Email,
                PromoCodes = result.PromoCodes.Select(q => new PromoCodeShortResponse
                {
                    Id = q.Id,
                    Code = q.Code,
                    PartnerName = q.PartnerName,
                    ServiceInfo = q.ServiceInfo,
                    BeginDate = q.BeginDate.ToString("d"),
                    EndDate = q.EndDate.ToString("d")
                }).ToList(),
            });
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //Сделать AsQuerable
            var existingPreferences = (await _preferenceRepository.GetAllAsync()).Where(q => request.PreferenceIds.Any(t => t.Equals(q.Id))).ToList();
            if (request.PreferenceIds.Any(q => !existingPreferences.Any(t => t.Id.Equals(q)))) return BadRequest();
            var newCustomerId = await _customerRepository.AddAsync(new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = existingPreferences
            });
            return Ok(newCustomerId);
        }
        
        /// <summary>
        /// Редактировать клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null) return NotFound();
            //Сделать AsQuerable
            var existingPreferences = (await _preferenceRepository.GetAllAsync()).Where(q => request.PreferenceIds.Any(t => t.Equals(q.Id))).ToList();
            if (request.PreferenceIds.Any(q => !existingPreferences.Any(t => t.Id.Equals(q)))) return BadRequest();

            await _customerRepository.UpdateAsync(new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = existingPreferences,
                PromoCodes = customer.PromoCodes
            });
            return Ok();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            if (!(await _customerRepository.IsExistsAsync(id))) return NotFound();
            await _customerRepository.RemoveByIdAsync(id);
            return Ok();
        }
    }
}