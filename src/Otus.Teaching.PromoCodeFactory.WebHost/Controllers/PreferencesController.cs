﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.EfRepositoryImplementations;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using SQLitePCL;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferencesController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }


        /// <summary>
        /// Получить данные всех предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PreferenceShortResponse>> GetPreferencesAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var employeesModelList = preferences.Select(x =>
                new PreferenceShortResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                }).ToList();
            return employeesModelList;
        }

        /// <summary>
        /// Получить данные предпочтения по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PreferenceResponse>> GetPreferenceByIdAsync(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            if (preference == null) return NotFound();

            return new PreferenceResponse()
            {
                Id = preference.Id,
                Name = preference.Name,
                Customers = preference.Customers.Select(q => new CustomerShortResponse
                {
                    Id = q.Id,
                    FirstName = q.FirstName,
                    LastName = q.LastName,
                    Email = q.Email
                }).ToList()
            };
        }
    }
}